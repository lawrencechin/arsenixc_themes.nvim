# Arsenixc_Themes.nvim
![Arsenixc Themes header image](./media/Arsenixc_Themes.png)

A set of *light* and **dark** themes based on the artwork of [Arsenixc](https://www.deviantart.com/arsenixc) written in *lua*.

Includes colour themes for [Kitty](https://sw.kovidgoyal.net/kitty/).

If you're in need of a breather, check out my other theme [**fresh air**](https://gitlab.com/lawrencechin/fresh_air.nvim).

## Included Themes

![Theme Colours Screenshot](./media/all_themes.png)

Theme Name | Code Name
-- | --
Akihabara South Exit Night | `akihabara_south_exit_night`
Akihabara South Exit | `akihabara_south_exit`
Himitsu House Day | `himitsu_house_day`
Himitsu House Night | `himitsu_house_night`
Japanese Garden Dark | `japanese_garden_dark`
Japanese Garden Light | `japanese_garden_light`
Old Bar | `old_bar`
Riverside Day | `riverside_day`
Riverside Night | `riverside_night`
Riverside Nightlight | `riverside_nightlight`
School Entry Light | `school_entry_light`
School Entry_Dark | `school_entry_dark`
Sky City | `sky_city`
Tokyo Street Night | `tokyo_street_night`
Tokyo Street Sunset Dark | `tokyo_street_sunset_dark`
Tokyo Street Sunset Light | `tokyo_street_sunset_light`
Tokyo Street Sunset | `tokyo_street_sunset`
Tokyo Street | `toyko_street`
Ultima Bar | `ultima_bar`
Wild Night Club | `wild_night_club`

## Installation & Usage

> Requires Neovim >= 0.8.0

Install via a package manager:

``` lua
require "paq" {
    { url = "https://gitlab.com/lawrencechin/arsenix_themes.nvim" };
}
```

Enable the colour scheme:

``` vim
" Vimscript
colorscheme arsenixc
```

``` lua
-- Lua
require( "arsenixc" ).setup()
```

To select a specific theme:

``` vim
" Vimscript
" default value: tokyo_street
let g:arsenixc_style = "$style"
```

``` lua
-- Lua
-- default value: tokyo_street
vim.g.arsenixc_style = "$style"
```

To enable a transparent background:

``` vim
" Vimscript
" default value: false
let g:arsenixc_transparent_background = true
```

``` lua
-- Lua
-- default value: false
vim.g.arsenixc_transparent_background = true
```

To cycle through themes:

``` vim
:lua require("arsenixc.functions").change_style("$style")
" Alternatively, use the following after loading the scheme to save undue typing
:Arsenixc <TAB> $style
```

To switch themes based on the current system theme (**dark**/*light*) take a ganders at [Dark Notify](https://github.com/cormacrelf/dark-notify/)

![Thanks for Viewing](./media/a1_75.gif)
