lua << EOF

package.loaded[ 'arsenixc' ]        = nil
package.loaded[ 'arsenixc.utils' ]  = nil
package.loaded[ 'arsenixc.function' ] = nil
package.loaded[ 'arsenixc.colours' ] = nil
package.loaded[ 'arsenixc.scheme' ] = nil

require( "arsenixc" ).setup()

EOF
