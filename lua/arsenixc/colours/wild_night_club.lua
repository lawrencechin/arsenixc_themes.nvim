local Wild_Night = {}

Wild_Night.Background_Colour = "#1A1609"
Wild_Night.Black = "#000000"
Wild_Night.Bold_Italic = "#C2BABB"
Wild_Night.Blue = "#5A8FC8"
Wild_Night.Green = "#86CA82"
Wild_Night.Grey = "#876AA4"
Wild_Night.Magenta = "#F25FF4"
Wild_Night.Mauve = "#9575B8"
Wild_Night.Orange = "#E73A58"
Wild_Night.Pink = "#EC7599"
Wild_Night.Popup_Colour = "#393013"
Wild_Night.Purple = "#A35993"
Wild_Night.Red = "#F74049"
Wild_Night.Slate_Grey = "#60719F"
Wild_Night.Strong_Purple = "#E56470"
Wild_Night.Text_Colour = "#ECE3E4"
Wild_Night.White = "#F5FFFF"
Wild_Night.Yellow = "#F1D560"

return Wild_Night

