local Old_Bar = {}

Old_Bar.Background_Colour = "#2B221F"
Old_Bar.Black = "#000000"
Old_Bar.Bold_Italic = "#D0BAB4"
Old_Bar.Blue = "#B49FBE"
Old_Bar.Green = "#BEB24C"
Old_Bar.Grey = "#867F87"
Old_Bar.Magenta = "#DB6F6B"
Old_Bar.Mauve = "#C6A9D6"
Old_Bar.Orange = "#C6521E"
Old_Bar.Pink = "#C18161"
Old_Bar.Popup_Colour = "#56443E"
Old_Bar.Purple = "#CD72CD"
Old_Bar.Red = "#D9AD72"
Old_Bar.Slate_Grey = "#C26B52"
Old_Bar.Strong_Purple = "#BE91E3"
Old_Bar.Text_Colour = "#F3D9D2"
Old_Bar.White = "#F5FFFF"
Old_Bar.Yellow = "#BEB24C"

return Old_Bar
