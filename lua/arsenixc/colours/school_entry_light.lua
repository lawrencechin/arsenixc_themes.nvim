local School_Entry_Light = {}

School_Entry_Light.Background_Colour = "#FBFBFE"
School_Entry_Light.Black = "#323850"
School_Entry_Light.Bold_Italic = "#323850"
School_Entry_Light.Blue = "#4763A7"
School_Entry_Light.Green = "#3A6A42"
School_Entry_Light.Grey = "#6E7A86"
School_Entry_Light.Magenta = "#A5475A"
School_Entry_Light.Mauve = "#A08B73"
School_Entry_Light.Orange = "#D06656"
School_Entry_Light.Pink = "#EA80C1"
School_Entry_Light.Popup_Colour = "#F4F7FC"
School_Entry_Light.Purple = "#7E64DC"
School_Entry_Light.Red = "#BE2D37"
School_Entry_Light.Slate_Grey = "#438EA8"
School_Entry_Light.Strong_Purple = "#603870"
School_Entry_Light.Text_Colour = "#2C3047"
School_Entry_Light.White = "#FBFBFE"
School_Entry_Light.Yellow = "#EDCB68"

return School_Entry_Light
