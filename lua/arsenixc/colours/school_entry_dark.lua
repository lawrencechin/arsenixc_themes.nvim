local School_Entry_Dark = {}

School_Entry_Dark.Background_Colour = "#101933"
School_Entry_Dark.Black = "#262734"
School_Entry_Dark.Bold_Italic = "#FAFCF9"
School_Entry_Dark.Blue = "#95C4F2"
School_Entry_Dark.Green = "#90C88D"
School_Entry_Dark.Grey = "#CAD8D8"
School_Entry_Dark.Magenta = "#AA6466"
School_Entry_Dark.Mauve = "#D6C8DE"
School_Entry_Dark.Orange = "#E78351"
School_Entry_Dark.Pink = "#EA80C1"
School_Entry_Dark.Popup_Colour = "#2F3948"
School_Entry_Dark.Purple = "#BD80E0"
School_Entry_Dark.Red = "#D06656"
School_Entry_Dark.Slate_Grey = "#58CCBF"
School_Entry_Dark.Strong_Purple = "#B77BB4"
School_Entry_Dark.Text_Colour = "#FBFBFE"
School_Entry_Dark.White = "#FBFBFE"
School_Entry_Dark.Yellow = "#EDCB68"

return School_Entry_Dark
