local Tokyo_Street = {}

Tokyo_Street.Background_Colour = "#F8F8F8"
Tokyo_Street.Black = "#000000"
Tokyo_Street.Bold_Italic = "#535353"
Tokyo_Street.Blue = "#107EDB"
Tokyo_Street.Green = "#60A076"
Tokyo_Street.Grey = "#687D8B"
Tokyo_Street.Magenta = "#BD73C5"
Tokyo_Street.Mauve = "#B6898E"
Tokyo_Street.Orange = "#EA6D33"
Tokyo_Street.Pink = "#1466B1"
Tokyo_Street.Popup_Colour = "#E7E3E3"
Tokyo_Street.Purple = "#8D546C"
Tokyo_Street.Red = "#E03843"
Tokyo_Street.Slate_Grey = "#6E6359"
Tokyo_Street.Strong_Purple = "#394D65"
Tokyo_Street.Text_Colour = "#232323"
Tokyo_Street.White = "#F5ECEA"
Tokyo_Street.Yellow = "#D1A64A"

return Tokyo_Street
