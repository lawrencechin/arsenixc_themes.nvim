local Riverside_Night = {}

Riverside_Night.Background_Colour = "#1D273B"
Riverside_Night.Black = "#000000"
Riverside_Night.Bold_Italic = "#E4E1D9"
Riverside_Night.Blue = "#6587CE"
Riverside_Night.Green = "#29C5A9"
Riverside_Night.Grey = "#A79579"
Riverside_Night.Magenta = "#E478C1"
Riverside_Night.Mauve = "#BB9298"
Riverside_Night.Orange = "#FCA850"
Riverside_Night.Pink = "#F5959F"
Riverside_Night.Popup_Colour = "#32456A"
Riverside_Night.Purple = "#5C689E"
Riverside_Night.Red = "#EE3A42"
Riverside_Night.Slate_Grey = "#AF8E7E"
Riverside_Night.Strong_Purple = "#8B9CE9"
Riverside_Night.Text_Colour = "#FBF7EE"
Riverside_Night.White = "#F5F2F1"
Riverside_Night.Yellow = "#E2BE6F"

return Riverside_Night
