local Akihabara_SE = {}

Akihabara_SE.Background_Colour = "#FCFAF7"
Akihabara_SE.Black = "#000000"
Akihabara_SE.Blue = "#3994C1"
Akihabara_SE.Bold_Italic = "#3D5480"
Akihabara_SE.Green = "#3B6F46"
Akihabara_SE.Grey = "#C5A179"
Akihabara_SE.Magenta = "#B95089"
Akihabara_SE.Mauve = "#A48F6C"
Akihabara_SE.Orange = "#D08D45"
Akihabara_SE.Pink = "#A78999"
Akihabara_SE.Popup_Colour = "#F8F1E7"
Akihabara_SE.Purple = "#5459AB"
Akihabara_SE.Red = "#E06466"
Akihabara_SE.Slate_Grey = "#3B5B95"
Akihabara_SE.Strong_Purple = "#393D64"
Akihabara_SE.Text_Colour = "#2E3E5C"
Akihabara_SE.White = "#F2F3F8"
Akihabara_SE.Yellow = "#BB9974"

return Akihabara_SE
