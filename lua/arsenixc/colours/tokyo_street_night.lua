local Tokyo_Street_Night = {}

Tokyo_Street_Night.Background_Colour = "#121D3D"
Tokyo_Street_Night.Black = "#000000"
Tokyo_Street_Night.Bold_Italic = "#FDF6F1"
Tokyo_Street_Night.Blue = "#8090D8"
Tokyo_Street_Night.Green = "#4D8897"
Tokyo_Street_Night.Grey = "#E6D9CC"
Tokyo_Street_Night.Magenta = "#A8603F"
Tokyo_Street_Night.Mauve = "#B9A8EC"
Tokyo_Street_Night.Orange = "#BD8140"
Tokyo_Street_Night.Pink = "#B97C85"
Tokyo_Street_Night.Popup_Colour = "#203674"
Tokyo_Street_Night.Purple = "#527EFF"
Tokyo_Street_Night.Red = "#BD3C54"
Tokyo_Street_Night.Slate_Grey = "#E2D3C9"
Tokyo_Street_Night.Strong_Purple = "#7F9BE5"
Tokyo_Street_Night.Text_Colour = "#FEEDE1"
Tokyo_Street_Night.White = "#FEFFFF"
Tokyo_Street_Night.Yellow = "#D7BB7E"

return Tokyo_Street_Night
