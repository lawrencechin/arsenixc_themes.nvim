local Akihabara_SE_Night = {}

Akihabara_SE_Night.Background_Colour = "#25292D"
Akihabara_SE_Night.Black = "#000000"
Akihabara_SE_Night.Bold_Italic = "#DBD4D7"
Akihabara_SE_Night.Slate_Grey = "#84A7CF"
Akihabara_SE_Night.Green = "#579355"
Akihabara_SE_Night.Grey = "#6F675B"
Akihabara_SE_Night.Magenta = "#CD49A0"
Akihabara_SE_Night.Mauve = "#A78999"
Akihabara_SE_Night.Orange = "#FAED7C"
Akihabara_SE_Night.Pink = "#AB415A"
Akihabara_SE_Night.Popup_Colour = "#3F454C"
Akihabara_SE_Night.Purple = "#C35BC9"
Akihabara_SE_Night.Red = "#E83748"
Akihabara_SE_Night.Blue = "#427FE7"
Akihabara_SE_Night.Strong_Purple = "#D47F9E"
Akihabara_SE_Night.Text_Colour = "#EBE5E8"
Akihabara_SE_Night.White = "#F2ECF4"
Akihabara_SE_Night.Yellow = "#FAED7C"

return Akihabara_SE_Night
