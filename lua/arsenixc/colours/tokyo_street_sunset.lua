local Tokyo_Street_Sunset = {}

Tokyo_Street_Sunset.Background_Colour = "#332620"
Tokyo_Street_Sunset.Black = "#000000"
Tokyo_Street_Sunset.Bold_Italic = "#E3D7DC"
Tokyo_Street_Sunset.Blue = "#6781D5"
Tokyo_Street_Sunset.Green = "#908767"
Tokyo_Street_Sunset.Grey = "#B0A9B3"
Tokyo_Street_Sunset.Magenta = "#AE6684"
Tokyo_Street_Sunset.Mauve = "#ECAC86"
Tokyo_Street_Sunset.Orange = "#DF7143"
Tokyo_Street_Sunset.Pink = "#E7B5EA"
Tokyo_Street_Sunset.Popup_Colour = "#5A4B42"
Tokyo_Street_Sunset.Purple = "#B36988"
Tokyo_Street_Sunset.Red = "#E9482F"
Tokyo_Street_Sunset.Slate_Grey = "#839891"
Tokyo_Street_Sunset.Strong_Purple = "#B99BE1"
Tokyo_Street_Sunset.Text_Colour = "#FEF0F6"
Tokyo_Street_Sunset.White = "#F7F4EE"
Tokyo_Street_Sunset.Yellow = "#F6D958"

return Tokyo_Street_Sunset
