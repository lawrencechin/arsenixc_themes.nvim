local Japanese_Garden_Light = {}

Japanese_Garden_Light.Background_Colour = "#FDFFFF"
Japanese_Garden_Light.Black = "#052736"
Japanese_Garden_Light.Blue = "#0D6BB4"
Japanese_Garden_Light.Bold_Italic = "#0C3E27"
Japanese_Garden_Light.Green = "#186B54"
Japanese_Garden_Light.Grey = "#9DAEB8"
Japanese_Garden_Light.Magenta = "#C42254"
Japanese_Garden_Light.Mauve = "#CC8CA4"
Japanese_Garden_Light.Orange = "#F75630"
Japanese_Garden_Light.Pink = "#D07777"
Japanese_Garden_Light.Popup_Colour = "#F9FAFC"
Japanese_Garden_Light.Purple = "#8476C8"
Japanese_Garden_Light.Red = "#D3263B"
Japanese_Garden_Light.Slate_Grey = "#0A4654"
Japanese_Garden_Light.Strong_Purple = "#4042BC"
Japanese_Garden_Light.Text_Colour = "#06343C"
Japanese_Garden_Light.White = "#FDFDFD"
Japanese_Garden_Light.Yellow = "#D89A2B"

return Japanese_Garden_Light
