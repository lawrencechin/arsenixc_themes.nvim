local Colours = {}
local variant = vim.g.arsenixc_style

---Variants selector
if variant == "akihabara_south_exit" then
    Colours = require( "arsenixc.colours.akihabara_south_exit" )
elseif variant == "akihabara_south_exit_night" then
    Colours = require( "arsenixc.colours.akihabara_south_exit_night" )
elseif variant == "old_bar" then
    Colours = require( "arsenixc.colours.old_bar" )
elseif variant == "riverside_day" then
    Colours = require( "arsenixc.colours.riverside_day" )
elseif variant == "riverside_night" then
    Colours = require( "arsenixc.colours.riverside_night" )
elseif variant == "riverside_nightlight" then
    Colours = require( "arsenixc.colours.riverside_nightlight" )
elseif variant == "sky_city" then
    Colours = require( "arsenixc.colours.sky_city" )
elseif variant == "tokyo_street" then
    Colours = require( "arsenixc.colours.tokyo_street" )
elseif variant == "tokyo_street_night" then
    Colours = require( "arsenixc.colours.tokyo_street_night" )
elseif variant == "tokyo_street_sunset" then
    Colours = require( "arsenixc.colours.tokyo_street_sunset" )
elseif variant == "tokyo_street_sunset_light" then
    Colours = require( "arsenixc.colours.tokyo_street_sunset_light" )
elseif variant == "tokyo_street_sunset_night" then
    Colours = require( "arsenixc.colours.tokyo_street_sunset_night" )
elseif variant == "ultima_bar" then
    Colours = require( "arsenixc.colours.ultima_bar" )
elseif variant == "wild_night_club" then
    Colours = require( "arsenixc.colours.wild_night_club" )
elseif variant == "himitsu_house_day" then
    Colours = require( "arsenixc.colours.himitsu_house_day" )
elseif variant == "himitsu_house_night" then
    Colours = require( "arsenixc.colours.himitsu_house_night" )
elseif variant == "japanese_garden_dark" then
    Colours = require( "arsenixc.colours.japanese_garden_dark" )
elseif variant == "japanese_garden_light" then
    Colours = require( "arsenixc.colours.japanese_garden_light" )
elseif variant == "school_entry_dark" then
    Colours = require( "arsenixc.colours.school_entry_dark" )
elseif variant == "school_entry_light" then
    Colours = require( "arsenixc.colours.school_entry_light" )
else
    Colours = require( "arsenixc.colours.tokyo_street") --Default schema
end

Colours.none = "NONE"

return Colours
