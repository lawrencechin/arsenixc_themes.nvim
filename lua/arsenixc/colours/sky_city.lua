local Sky_City = {}

Sky_City.Background_Colour = "#181E39"
Sky_City.Black = "#000000"
Sky_City.Bold_Italic = "#CFC1C3"
Sky_City.Blue = "#6D88FB"
Sky_City.Green = "#C5E2A5"
Sky_City.Grey = "#E7D3BE"
Sky_City.Magenta = "#B85368"
Sky_City.Mauve = "#9895B6"
Sky_City.Orange = "#ED9450"
Sky_City.Pink = "#CB98A8"
Sky_City.Popup_Colour = "#2B376B"
Sky_City.Purple = "#BEB1F9"
Sky_City.Red = "#E08A99"
Sky_City.Slate_Grey = "#59D0FD"
Sky_City.Strong_Purple = "#B48FCB"
Sky_City.Text_Colour = "#F4E3E6"
Sky_City.White = "#F5FFFF"
Sky_City.Yellow = "#ED9450"

return Sky_City
