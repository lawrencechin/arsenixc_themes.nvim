local Japanese_Garden_Dark = {}

Japanese_Garden_Dark.Background_Colour = "#052238"
Japanese_Garden_Dark.Black = "#052736"
Japanese_Garden_Dark.Blue = "#8ABFEF"
Japanese_Garden_Dark.Bold_Italic = "#F9FAFC"
Japanese_Garden_Dark.Green = "#45BC74"
Japanese_Garden_Dark.Grey = "#BFCBDD"
Japanese_Garden_Dark.Magenta = "#C97F85"
Japanese_Garden_Dark.Mauve = "#DFD0DA"
Japanese_Garden_Dark.Orange = "#EC742B"
Japanese_Garden_Dark.Pink = "#D59594"
Japanese_Garden_Dark.Popup_Colour = "#06343C"
Japanese_Garden_Dark.Purple = "#C77BB3"
Japanese_Garden_Dark.Red = "#CA3749"
Japanese_Garden_Dark.Slate_Grey = "#3CA19B"
Japanese_Garden_Dark.Strong_Purple = "#8476C8"
Japanese_Garden_Dark.Text_Colour = "#FDFDFD"
Japanese_Garden_Dark.White = "#FDFDFD"
Japanese_Garden_Dark.Yellow = "#F9DB5A"

return Japanese_Garden_Dark
