local Tokyo_Street_Sunset_Light = {}

Tokyo_Street_Sunset_Light.Background_Colour = "#F8F8FA"
Tokyo_Street_Sunset_Light.Black = "#000000"
Tokyo_Street_Sunset_Light.Bold_Italic = "#452A3E"
Tokyo_Street_Sunset_Light.Blue = "#4055AA"
Tokyo_Street_Sunset_Light.Green = "#8E865B"
Tokyo_Street_Sunset_Light.Grey = "#8A6F76"
Tokyo_Street_Sunset_Light.Magenta = "#AE6684"
Tokyo_Street_Sunset_Light.Mauve = "#F08570"
Tokyo_Street_Sunset_Light.Orange = "#AA4B44"
Tokyo_Street_Sunset_Light.Pink = "#ED97B2"
Tokyo_Street_Sunset_Light.Popup_Colour = "#FAF2F2"
Tokyo_Street_Sunset_Light.Purple = "#82547A"
Tokyo_Street_Sunset_Light.Red = "#E23B2A"
Tokyo_Street_Sunset_Light.Slate_Grey = "#3F3B50"
Tokyo_Street_Sunset_Light.Strong_Purple = "#6C486A"
Tokyo_Street_Sunset_Light.Text_Colour = "#4E384F"
Tokyo_Street_Sunset_Light.White = "#F7F4EE"
Tokyo_Street_Sunset_Light.Yellow = "#DFA34A"

return Tokyo_Street_Sunset_Light
