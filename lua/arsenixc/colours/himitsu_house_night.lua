local Himitsu_House_Night = {}

Himitsu_House_Night.Background_Colour = "#102B3E"
Himitsu_House_Night.Black = "#2D3A34"
Himitsu_House_Night.Bold_Italic = "#F2E9D1"
Himitsu_House_Night.Blue = "#4490A7"
Himitsu_House_Night.Green = "#388676"
Himitsu_House_Night.Grey = "#2A75A5"
Himitsu_House_Night.Magenta = "#BC85D4"
Himitsu_House_Night.Mauve = "#B07CE7"
Himitsu_House_Night.Orange = "#E9A64A"
Himitsu_House_Night.Pink = "#C89695"
Himitsu_House_Night.Popup_Colour = "#4D6894"
Himitsu_House_Night.Purple = "#727FF8"
Himitsu_House_Night.Red = "#ED7C70"
Himitsu_House_Night.Slate_Grey = "#1777A7"
Himitsu_House_Night.Strong_Purple = "#FDD38E"
Himitsu_House_Night.Text_Colour = "#F2F0E7"
Himitsu_House_Night.White = "#F2E4BB"
Himitsu_House_Night.Yellow = "#FAF36E"

return Himitsu_House_Night
