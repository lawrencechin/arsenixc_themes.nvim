local Ultima_Bar = {}

Ultima_Bar.Background_Colour = "#16132C"
Ultima_Bar.Black = "#000000"
Ultima_Bar.Bold_Italic = "#BFBABE"
Ultima_Bar.Blue = "#A0A5FF"
Ultima_Bar.Green = "#9BBD4D"
Ultima_Bar.Grey = "#B681A6"
Ultima_Bar.Magenta = "#A35186"
Ultima_Bar.Mauve = "#A0A5FF"
Ultima_Bar.Orange = "#F4B56A"
Ultima_Bar.Pink = "#C69E7F"
Ultima_Bar.Popup_Colour = "#292354"
Ultima_Bar.Purple = "#A96DF1"
Ultima_Bar.Red = "#E24F58"
Ultima_Bar.Slate_Grey = "#9178AA"
Ultima_Bar.Strong_Purple = "#958AF4"
Ultima_Bar.Text_Colour = "#E6DFE4"
Ultima_Bar.White = "#F5FFFF"
Ultima_Bar.Yellow = "#F4B56A"

return Ultima_Bar

