local Tokyo_Street_Sunset_Night = {}

Tokyo_Street_Sunset_Night.Background_Colour = "#1C1512"
Tokyo_Street_Sunset_Night.Black = "#000000"
Tokyo_Street_Sunset_Night.Bold_Italic = "#E3D7DC"
Tokyo_Street_Sunset_Night.Blue = "#6781D5"
Tokyo_Street_Sunset_Night.Green = "#908767"
Tokyo_Street_Sunset_Night.Grey = "#B0A9B3"
Tokyo_Street_Sunset_Night.Magenta = "#AE6684"
Tokyo_Street_Sunset_Night.Mauve = "#ECAC86"
Tokyo_Street_Sunset_Night.Orange = "#DF7143"
Tokyo_Street_Sunset_Night.Pink = "#E7B5EA"
Tokyo_Street_Sunset_Night.Popup_Colour = "#5A4B42"
Tokyo_Street_Sunset_Night.Purple = "#B36988"
Tokyo_Street_Sunset_Night.Red = "#E9482F"
Tokyo_Street_Sunset_Night.Slate_Grey = "#839891"
Tokyo_Street_Sunset_Night.Strong_Purple = "#B99BE1"
Tokyo_Street_Sunset_Night.Text_Colour = "#FEF0F6"
Tokyo_Street_Sunset_Night.White = "#F7F4EE"
Tokyo_Street_Sunset_Night.Yellow = "#F6D958"

return Tokyo_Street_Sunset_Night
