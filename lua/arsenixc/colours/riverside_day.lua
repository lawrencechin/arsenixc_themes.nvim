local Riverside_day = {}

Riverside_day.Background_Colour = "#F9FCFB"
Riverside_day.Black = "#000000"
Riverside_day.Bold_Italic = "#697471"
Riverside_day.Blue = "#2B74D0"
Riverside_day.Green = "#528C7B"
Riverside_day.Grey = "#52505F"
Riverside_day.Magenta = "#885750"
Riverside_day.Mauve = "#59BAFE"
Riverside_day.Orange = "#CEA27E"
Riverside_day.Pink = "#E194A2"
Riverside_day.Popup_Colour = "#E0E7E6"
Riverside_day.Purple = "#60446E"
Riverside_day.Red = "#B15868"
Riverside_day.Slate_Grey = "#556484"
Riverside_day.Strong_Purple = "#5E5487"
Riverside_day.Text_Colour = "#34443F"
Riverside_day.White = "#F9FBFF"
Riverside_day.Yellow = "#CFBE54"

return Riverside_day
