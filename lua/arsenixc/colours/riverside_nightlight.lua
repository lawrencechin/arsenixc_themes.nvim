local Riverside_NightLight = {}

Riverside_NightLight.Background_Colour = "#F9FBFF"
Riverside_NightLight.Black = "#000000"
Riverside_NightLight.Bold_Italic = "#4D5269"
Riverside_NightLight.Blue = "#6587CE"
Riverside_NightLight.Green = "#29C5A9"
Riverside_NightLight.Grey = "#A79579"
Riverside_NightLight.Magenta = "#A8603F"
Riverside_NightLight.Mauve = "#BB9298"
Riverside_NightLight.Orange = "#FCA850"
Riverside_NightLight.Pink = "#F5959F"
Riverside_NightLight.Popup_Colour = "#DDE0E5"
Riverside_NightLight.Purple = "#5C689E"
Riverside_NightLight.Red = "#EE3A42"
Riverside_NightLight.Slate_Grey = "#AF8E7E"
Riverside_NightLight.Strong_Purple = "#8B9CE9"
Riverside_NightLight.Text_Colour = "#2E313F"
Riverside_NightLight.White = "#F5F2F1"
Riverside_NightLight.Yellow = "#E2BE6F"

return Riverside_NightLight
