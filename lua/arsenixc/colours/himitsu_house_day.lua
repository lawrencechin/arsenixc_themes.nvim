local Himitsu_House_Day = {}

Himitsu_House_Day.Background_Colour = "#F8F4F0"
Himitsu_House_Day.Black = "#172821"
Himitsu_House_Day.Bold_Italic = "#1F475A"
Himitsu_House_Day.Blue = "#4B9CF7"
Himitsu_House_Day.Green = "#3A732C"
Himitsu_House_Day.Grey = "#717E9B"
Himitsu_House_Day.Magenta = "#AD6F3C"
Himitsu_House_Day.Mauve = "#A5929F"
Himitsu_House_Day.Orange = "#BB7667"
Himitsu_House_Day.Pink = "#D0A492"
Himitsu_House_Day.Popup_Colour = "#F9E9D9"
Himitsu_House_Day.Purple = "#4B6DB2"
Himitsu_House_Day.Red = "#C65133"
Himitsu_House_Day.Slate_Grey = "#446071"
Himitsu_House_Day.Strong_Purple = "#2F5067"
Himitsu_House_Day.Text_Colour = "#172820"
Himitsu_House_Day.White = "#F9EEF6"
Himitsu_House_Day.Yellow = "#D5A638"

return Himitsu_House_Day
