local scheme = require( "arsenixc.scheme" )
local utils = {}
local api = vim.api

local colour_tables = {
    scheme.load_editor(),
    scheme.load_plugins(),
    scheme.load_langs()
}

-- no longer used, prefering lua api nvim_set_sl
---Apply colors in the editor
function utils.set_highlights(group, color)

    api.nvim_command(string.format('highlight %s gui=%s guifg=%s guibg=%s guisp=%s',
        group,
        color.style or "NONE",
        color.fg    or "NONE",
        color.bg    or "NONE",
        color.sp    or "NONE"
    ))

    if color.link then
        api.nvim_command(string.format("highlight! link %s %s", group, color.link))
    end
end

---Load colorscheme
function utils.load_colorscheme()
    api.nvim_command("highlight clear")
    if vim.fn.exists("syntax_on") then 
        api.nvim_command("syntax reset") 
    end
    vim.o.termguicolors = true
    vim.g.colors_name = "arsenixc"

    -- Load terminal colors
    scheme.colors_terminal()
    -- Load editor colors
    for _, c_table in pairs(colour_tables) do
        for group, colour in pairs(c_table) do
            api.nvim_set_hl( 0, group, colour )
        end
    end
end

return utils
