local utils = require( "arsenixc.utils" )
local functions = require( "arsenixc.functions" )
local arsenixc = {}

function arsenixc.setup()
    utils.load_colorscheme()
end

return arsenixc
