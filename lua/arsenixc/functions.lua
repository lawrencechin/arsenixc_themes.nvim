local function change_style( style )
    vim.g.arsenixc_style = style
    vim.cmd[[colorscheme arsenixc]]
end

local function list_themes()
    local themes = {
        "akihabara_south_exit",
        "akihabara_south_exit_night",
        "himitsu_house_day",
        "himitsu_house_night",
        "japanese_garden_dark",
        "japanese_garden_light",
        "old_bar",
        "riverside_day",
        "riverside_night",
        "riverside_nightlight",
        "school_entry_dark",
        "school_entry_light",
        "sky_city",
        "tokyo_street",
        "tokyo_street_night",
        "tokyo_street_sunset",
        "tokyo_street_sunset_light",
        "tokyo_street_sunset_night",
        "ultima_bar",
        "wild_night_club",
    }

    return themes
end

vim.api.nvim_create_user_command(
    "Arsenixc",
    function( opts )
        change_style( opts.args )
    end,
    {
        nargs = 1,
        complete = list_themes
    }
)

local M = {}

M.change_style = change_style
M.list_themes = list_themes

return M
